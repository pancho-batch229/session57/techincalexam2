let collection = [];
// Write the queue functions below.

function print(){
    return collection;
}

function enqueue(item){
    // add an item
    collection.push(item);
    return collection;
}

function dequeue(){
    // remove an item
    collection.shift();
    return collection;
}

function front(){
    // get the front item
    let firstItem = collection[0];
    return firstItem;
}

function size(){
    // get the size of the array
    let araryLength = collection.length;
    return araryLength;
}

function isEmpty(){
    // check array if empty
    let araryLength = collection.length;
    let isEmpty;

    if(araryLength > 0) {
        isEmpty = false;
    } else {
        isEmpty = true;
    }

    return isEmpty;
}






// Export create queue functions below.
module.exports = {
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};